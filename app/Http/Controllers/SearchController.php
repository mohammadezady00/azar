<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Foundation\Application;
use Inertia\Inertia;
use Illuminate\Support\Facades\Route;
use \DetectLanguage\DetectLanguage;
use Orhanerday\OpenAi\OpenAi;
use App\Models\Search;
use App\Models\History;
use App\Models\PopularSearchs;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class SearchController extends Controller
{
    public function show($key_word)
    {
        $searches = Search::all();
        $historys = History::all();

        // save history
        if (! History::where('user_id', Auth::id())->where('search_text', $key_word)->first()) {
            if (Auth::user()) {
                History::create([
                    'search_text' => $key_word,
                    'user_id' => Auth::id()
                ]);
            }
        }
 
        // check if in database
        foreach ($searches as $search) {
            if ($key_word == $search->key_word) {
                 // get popular searches if this key word is in databse,
                // plus one to views and if not make it and set views to one 
                $popularPost = PopularSearchs::where('key_word', $key_word)->first();
                if ($popularPost) {
                    $views = $popularPost->views;
                    $popularPost->update([
                        "views" => $views + 1
                    ]);
                }else {
                    PopularSearchs::create([
                        "key_word" => $key_word,
                        "views" => 1
                    ]);
                }

                return Inertia::render('Search', [
                    "summarize" => $search->summarize,
                    "article" => $search->article,
                    "img" => $search->img_src,
                    "okay" => $search->key_word,
                ]);
            }
        }

        // set $key_word to array , for confirm inertia
        $ok = [$key_word];

        // brows keys
        $open_ai = new OpenAI("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");
        DetectLanguage::setApiKey("dd31506418887e4a23c11650c3367dd4");

        // get lang code
        $languageCode = DetectLanguage::simpleDetect($key_word);

        // requst to wikipedia
        $wiki = Http::get("https://".$languageCode.".wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&titles=".$key_word)->json()['query']['pages'];
        $wiki_result = array_values($wiki)[0];

        // requst to get img wikipedia
        $wiki_img = Http::get("https://".$languageCode.".wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=original&titles=".$key_word)->json()['query']['pages'];
        $wiki_img_result = array_values($wiki_img)[0];

        // if subject not in wiki pedia extract text is: key word
        if (!array_key_exists("extract", $wiki_result)) {
            $wiki_result["extract"] = $key_word;
        }

        // if subject not in wiki pedia images original image is: our logo
        if (! array_key_exists("original", $wiki_img_result)) {
            $wiki_img_result['original'] = ["source"=>"/img/azar.png"];
        }


        // if wiki result is to long we make it small 
         if (strlen($wiki_result["extract"]) > 390) {
            $split = explode('.',$wiki_result["extract"]);
            $wiki_result["extract"] = $split[0];
            if ($wiki_result["extract"] > 390) {
                $wiki_result["extract"] = substr($wiki_result["extract"], 0, 300); 
            }
         }

        // set wiki_result to extracted text
        $wiki_result = $wiki_result["extract"];

        // make requset text
        if ($languageCode == "fa") {
            $content_summarize = "توضیحی ای در مورد این متن ارائه دهید. متن: " . $wiki_result;
            $content_articel = "مقاله ای در مورد : " . $wiki_result;
        } else {
            $content_summarize = "explanation of this text . text is: " . $wiki_result;
            $content_articel = "Write an article about" . $wiki_result;
        }
        
        // make summarize request
        $summarize_chat = $open_ai->chat([
            'model' => 'gpt-3.5-turbo',
            'messages' => [
                [
                    "role" => "user",
                    "content" => $content_summarize
                ],
            ],
            'temperature' => 1.0,
            // 'max_tokens' => 4000,
            'frequency_penalty' => 0,
            'presence_penalty' => 0,
         ]);
        $summarize_data = json_decode($summarize_chat, true);
        $summarize = $summarize_data['choices'][0];

        // make article request
        $article_chat = $open_ai->chat([
            'model' => 'gpt-3.5-turbo',
            'messages' => [
                [
                    "role" => "user",
                    "content" => $content_articel
                ],
            ],
            'temperature' => 1.0,
            // 'max_tokens' => 4000,
            'frequency_penalty' => 0,
            'presence_penalty' => 0,
         ]);

        $article_data = json_decode($article_chat, true);
        $article = $article_data['choices'][0];

        // get popular searches if this key word is in databse,
        // plus one to views and if not make it and set views to one 
        $popularPost = PopularSearchs::where('key_word', $key_word)->first();
        if ($popularPost) {
            $views = $popularPost->views;
            $popularPost->update([
                "views" => $views + 1
            ]);
        }else {
            PopularSearchs::create([
                "key_word" => $key_word,
                "views" => 1
            ]);
        }

        // return view
        return Inertia::render('Search', [
            "summarize" => $summarize["message"]["content"],
            "article" => $article["message"]["content"],
            "img" => $wiki_img_result['original']["source"],
            "okay" => $ok[0],
        ]);

    }


    public function like(Request $request)
    {
        // validate request
        $request->validate([
            'key_word' => 'required|string|unique:searches',
            'summarize' => 'required|string',
            'article' => 'required|string',
            'img_src' => 'required|string',
        ]);

        // make it in searches tabel
        $search = Search::create([
            'key_word' => $request->key_word,
            'summarize' => $request->summarize,
            'article' => $request->article,
            'img_src' => $request->img_src,
        ]);

        return back();
    }

    public function top()
    {

        // we can do that too
        $date = Carbon::today()->subDays(7);
        $searches = PopularSearchs::where('created_at', '>=',$date)->orderBy('views', 'desc')->take(10)->get();

        return Inertia::render('Top', [
            "tops" => $searches
        ]);
        
    }
}